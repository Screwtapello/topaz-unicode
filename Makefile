all: topaz_unicode_ks13_regular.ttf topaz_unicode_ks13_bold.ttf topaz_unicode_ks13_regular.psf

tmp/topaz_unicode_ks13_regular.bdf: src/regular-glyphs.bdf src/regular-metadata.bdf
	bdflib-merge $^ $@

tmp/bold-%.bdf: src/regular-%.bdf
	bdflib-embolden --ignore-spacing $^ $@

tmp/topaz_unicode_ks13_bold.bdf: tmp/bold-glyphs.bdf src/bold-metadata.bdf
	bdflib-merge $^ $@

%.ttf: tmp/%.bdf
	rm -f $@
	java -jar BitsNPicas.jar convertbitmap -o $@ -f ttf $<

%.psf: tmp/%.bdf \
		/usr/share/bdf2psf/standard.equivalents \
		/usr/share/bdf2psf/ascii.set \
		/usr/share/bdf2psf/linux.set \
		/usr/share/bdf2psf/useful.set
	bdf2psf \
		$< \
		/usr/share/bdf2psf/standard.equivalents \
		/usr/share/bdf2psf/ascii.set+/usr/share/bdf2psf/linux.set+/usr/share/bdf2psf/useful.set \
		512 \
		$@
